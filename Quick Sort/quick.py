# -*- coding: utf-8 -*-
from random import sample


def partition(vector):
    
    return None, None


def quick(vector, test: bool=False):
    # vector: array de números 
    # test: booleano que determina se o resultado será apresentado a cada iteração.

    if len(vector) <= 1:
        return vector
    
    v1, v2 = partition(vector)
    v1 = quick(v1)
    v2 = quick(v2)

    return v1 + v2

if __name__ == '__main__':
    a = quick([91, 28, 63, 80, 84, 61, 27, 36, 55, 34, 83, 87], True)
    print(a)
