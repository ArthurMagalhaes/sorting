# -*- coding: utf-8 -*-
from random import sample


def selection(vector, test: bool=False, head=[], count=0):
    # vector: array de números 
    # test: booleano que determina se o resultado será apresentado a cada iteração.
    # head: array de números que não será afetado pelo algoritmo.
    # count: número da iteração para o caso da apresentação.

    tam = len(vector)
    if tam > 1:
        if test:
            print(f'{count}: {head + vector}')
        menor = min(vector)
        p = vector.index(menor)
        b = vector[0]
        vector[0] = menor
        vector[p] = b
        vector = [vector[0]] + selection(vector[1:], test=test, head=head+[vector[0]], count=count+1)
        return vector
    else:
        return vector


if __name__ == '__main__':
    a = selection([91, 28, 63, 80, 84, 61, 27, 36, 55, 34, 83, 87], True)
    # a = selection(sample(range(1, 100), 10), True)

