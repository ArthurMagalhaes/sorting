# -*- coding: utf-8 -*-
from random import sample


def merge(vector, test: bool=False):
    # vector: array de números 
    # test: booleano que determina se o resultado será apresentado a cada iteração.

    if len(vector) <= 1:
        return vector
    
    i = 0
    j = 0
    p = len(vector) // 2
    v1 = merge(vector[:p], test)
    v2 = merge(vector[p:], test)
    result = []

    while i < len(v1) and j < len(v2):
        if v1[i] < v2[j]:
            result.append(v1[i])
            i += 1
        else:
            result.append(v2[j])
            j += 1
    
    while i < len(v1):

            result.append(v1[i])
            i += 1

    while j < len(v2):
        result.append(v2[j])
        j += 1
    
    if test:
        print(f'{result}')
    
    return result


if __name__ == '__main__':
    a = merge([91, 28, 63, 80, 84, 61, 27, 36, 55, 34, 83, 87], True)
    print(a)
