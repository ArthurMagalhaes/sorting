# -*- coding: utf-8 -*-
from random import sample


def insertion(vector, test: bool=False):
    # vector: array de números 
    # test: booleano que determina se o resultado será apresentado a cada iteração.

    tam = len(vector)
    p = 0
    i = 0
    for j in range(1, tam):

        if vector[j] < vector[j-1]:
            if test:
                print(f'{i}: {vector}')
            for k in range(0, j):
                if vector[j] < vector[k]:
                    p = k
                    break
            if j == tam - 1:
                tail = []
            else:
                tail = vector[j+1:]

            vector = vector[0:p] + [vector[j]] + vector[p:j] + tail
            i += 1

    if test: print(f'{i}: {vector}')

    return vector


if __name__ == '__main__':
    a = insertion([91, 28, 63, 80, 84, 61, 27, 36, 55, 34, 83, 87], True)
    # a = insertion(sample(range(1, 100), 12), True)
