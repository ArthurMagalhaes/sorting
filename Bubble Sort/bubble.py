# -*- coding: utf-8 -*-
from random import sample


def bubble(vector, test: bool=False, tail=[], count=0):
    # vector: array de números 
    # test: booleano que determina se o resultado será apresentado a cada iteração.
    # tail: array de números que não será afetado pelo algoritmo.
    # count: número da iteração para o caso da apresentação.

    tam = len(vector)

    if test:
        print(f'{count}: {vector+tail}')

    for j in range(1, tam):
        if vector[j] < vector[j-1]:
            b = vector[j]
            vector[j] = vector[j-1]
            vector[j-1] = b

    if tam > 1:
        vector = bubble(vector[:-1], test, [vector[-1]]+tail, count+1) + [vector[-1]]
    return vector


a = bubble([91, 28, 63, 80, 84, 61, 27, 36, 55, 34, 83, 87], True)
# a = bubble(sample(range(1, 100), 10), True)
print(a)
